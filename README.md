# WebSVG
WebSVG is a JavaScript API created by Kai Karren with the goal to simplify the (dynamic) creation of SVG elements and animations on websites.

## Features
The SVGDrawer class allows you to add any basic SVG Element to a SVG tag in your html per JavaScript at any time.
The SVGAnimationBuilder class offers the possibility to create move- and transfrom animations for all provided SVG elements. 
The RandomGenerator class offers methods to create random circles and rectangles.
The RandomAnimationGenerator class allows you to create n random circles with a given animation or a random moving animation for each circle.

More features are added in irregular time intervals.
Feel free to use and extend it for your own projects.
However if you create interesting extensions I would like if you notify me about it. I would like to inculde this new features then in WebSVG or link to the extension(s).
