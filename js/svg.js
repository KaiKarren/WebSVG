/**
 * WebSVG
 * Version 1.2.1 March 2019
 * 
 * The goal of WebSVG API is to offer a quicker way to create SVG graphics on websites.
 * Could also be used to dynamically create SVG content.
 * 
 * Offers functionalities for all basic SVG elements like circles, rectangles and polygones
 * as well as methods to create animations with this elements.
 * 
 * created by Kai Karren
 * Copyright 2019
 * Website: kaikarren.de
 */

/**
 * Main class for all operations on a SVG element.
 */
class SVGDrawer{

    /**
     * 
     * @param {String} svgID The id of the SVG tag in the html file
     * the drawSpace of the class then refers to this SVG tag.
     */
    constructor(svgID){
        this.svgID = svgID;
        this.drawSpace = document.getElementById(this.svgID);
    };

    /**
     * 
     * @param {number} cx  The coordinates of the center point in x direction
     * @param {number} cy  The coordinates of the center point in y direction 
     * @param {number} r   The radius of the circle 
     * @param {String} style The style to apply to the circle
     */
    addCircle(cx, cy, r, style){

        var circle = document.createElementNS("http://www.w3.org/2000/svg","circle");

        circle.setAttribute("cx", cx);
        circle.setAttribute("cy", cy);
        circle.setAttribute("r", r);
        circle.style = style;    

        this.drawSpace.appendChild(circle);

    };

    /**
     * 
     * @param {*} x         The x coordinate of the rectangle
     * @param {*} y         The y coordinate of the rectangle
     * @param {*} rx        How much the edges are rounded in x direction
     * @param {*} ry        How much the edges are rounded in y direction
     * @param {*} width     The width of the rectangle
     * @param {*} height    The height of the rectangle
     * @param {*} style     The style to use
     */
    addRectangle(x, y, rx, ry, width, height, style){

        var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");

        rect.setAttribute("x", x);
        rect.setAttribute("y", y);
        rect.setAttribute("rx", rx);
        rect.setAttribute("ry", ry);
        rect.setAttribute("width", width);
        rect.setAttribute("height", height);
        rect.style = style;

        this.drawSpace.appendChild(rect);

    }

    addLine(x1, y1, x2, y2, style){

        var line = document.createElementNS("http://www.w3.org/2000/svg","line");

        line.setAttribute("x1",x1);
        line.setAttribute("x2",x2);
        line.setAttribute("y1",y1);
        line.setAttribute("y2",y2);
        line.style = style;

        this.drawSpace.appendChild(line);

    };

    addEllipse(cx, cy, rx, ry, style){

        var ellipse = document.createElementNS("http://www.w3.org/2000/svg","ellipse");

        ellipse.setAttribute("cx",cx);
        ellipse.setAttribute("cy",cy);
        ellipse.setAttribute("rx",rx);
        ellipse.setAttribute("ry",ry);
        ellipse.style = style;

        this.drawSpace.appendChild(ellipse);

    };

    addPath(data, style){

        var path = document.createElementNS("http://www.w3.org/2000/svg","path");

        path.setAttribute("d",data);
        path.style=style;
        this.drawSpace.appendChild(path);

    };

    addPolygon(points, style){

        var polygon = document.createElementNS("http://www.w3.org/2000/svg","polygon");

        polygon.setAttribute("points",points);
        polygon.style = style;

        this.drawSpace.appendChild(polygon);

    };

    addPolyline(points, style){

        var polyline = document.createElementNS("http://www.w3.org/2000/svg","polyline");

        polyline.setAttribute("points",points);
        polyline.style = style;

        this.drawSpace.appendChild(polyline);

    };

    addText(text, x, y, style){

        var t = document.createElementNS("http://www.w3.org/2000/svg","text");

        t.innerHTML = text;
        t.setAttribute("x",x);
        t.setAttribute("y",y);
        t.setAttribute("style",style);

        this.drawSpace.appendChild(t);

    };

    getDrawSpaceWidth(){
        return this.drawSpace.getAttribute("width");
    };

    getDrawSpaceHeight(){
        return this.drawSpace.getAttribute("height");
    }

}

class MoveAnimation{

    constructor(path,duration,repeatCount){
        this.path = path;
        this.duration = duration;
        this.repeatCount = repeatCount;
    }
}

class TransformAnimation{

    constructor(type, from, to, duration, repeatCount){
        this.type = type;
        this.from = from;
        this.to = to;
        this.duration = duration;
        this.repeatCount = repeatCount;
    }

}

class SVGAnimationBuilder{

    constructor(svgID){
        this.svgID = svgID;
        this.drawSpace = document.getElementById(this.svgID);
    };

    addMovingCircle(cx, cy, r, style, path, duration, repeatCount){

        var circle = document.createElementNS("http://www.w3.org/2000/svg","circle");

        circle.setAttribute("cx", cx);
        circle.setAttribute("cy", cy);
        circle.setAttribute("r", r);
        circle.style = style;    

        this.drawSpace.appendChild(circle);
        
        this.addMoveAnimation(circle, path, duration, repeatCount);

    };

    addMovingLine(x1, y1, x2, y2, style, path, duration, repeatCount){

        var line = document.createElementNS("http://www.w3.org/2000/svg","line");

        line.setAttribute("x1",x1);
        line.setAttribute("x2",x2);
        line.setAttribute("y1",y1);
        line.setAttribute("y2",y2);
        line.style = style;

        this.drawSpace.appendChild(line);

        this.addMoveAnimation(line, path, duration, repeatCount);

    };

    addMovingEllipse(cx, cy, rx, ry, style, path, duration, repeatCount){

        var ellipse = document.createElementNS("http://www.w3.org/2000/svg","ellipse");

        ellipse.setAttribute("cx",cx);
        ellipse.setAttribute("cy",cy);
        ellipse.setAttribute("rx",rx);
        ellipse.setAttribute("ry",ry);
        ellipse.style = style;

        this.drawSpace.appendChild(ellipse);

        this.addMoveAnimation(ellipse, path, duration, repeatCount);

    };

    addMovingPath(data, style, path, duration, repeatCount){

        var p = document.createElementNS("http://www.w3.org/2000/svg","path");

        p.setAttribute("d",data);
        p.style=style;
        this.drawSpace.appendChild(p);

        this.addMoveAnimation(p, path, duration, repeatCount);

    };

    addMovingPolygon(points, style, path, duration, repeatCount){

        var polygon = document.createElementNS("http://www.w3.org/2000/svg","polygon");

        polygon.setAttribute("points",points);
        polygon.style = style;

        this.drawSpace.appendChild(polygon);

        this.addMoveAnimation(polygon, path, duration, repeatCount);

    };

    addMovingPolyline(points, style, path, duration, repeatCount){

        var polyline = document.createElementNS("http://www.w3.org/2000/svg","polyline");

        polyline.setAttribute("points",points);
        polyline.style = style;

        this.drawSpace.appendChild(polyline);

        this.addMoveAnimation(polyline, path, duration, repeatCount);

    };

    addMovingText(text, x, y, style, path, duration, repeatCount){

        var t = document.createElementNS("http://www.w3.org/2000/svg","text");

        t.innerHTML = text;
        t.setAttribute("x",x);
        t.setAttribute("y",y);
        t.setAttribute("style",style);

        this.drawSpace.appendChild(t);

        this.addMoveAnimation(t, path, duration, repeatCount);

    };

    addTransformingCircle(cx, cy, r, style, type, from, to, duration, repeatCount){

        var circle = document.createElementNS("http://www.w3.org/2000/svg","circle");

        circle.setAttribute("cx", cx);
        circle.setAttribute("cy", cy);
        circle.setAttribute("r", r);
        circle.style = style;    

        this.drawSpace.appendChild(circle);
        
        this.addTransformAnimation(circle, type, from, to, duration, repeatCount);

    };

    addTransformingLine(x1, y1, x2, y2, style, type, from, to, duration, repeatCount){

        var line = document.createElementNS("http://www.w3.org/2000/svg","line");

        line.setAttribute("x1",x1);
        line.setAttribute("x2",x2);
        line.setAttribute("y1",y1);
        line.setAttribute("y2",y2);
        line.style = style;

        this.drawSpace.appendChild(line);

        this.addTransformAnimation(line, type, from, to, duration, repeatCount);

    };

    addTransformingEllipse(cx, cy, rx, ry, style, type, from, to, duration, repeatCount){

        var ellipse = document.createElementNS("http://www.w3.org/2000/svg","ellipse");

        ellipse.setAttribute("cx",cx);
        ellipse.setAttribute("cy",cy);
        ellipse.setAttribute("rx",rx);
        ellipse.setAttribute("ry",ry);
        ellipse.style = style;

        this.drawSpace.appendChild(ellipse);

        this.addTransformAnimation(ellipse, type, from, to, duration, repeatCount);

    };

    addTransformingPath(data, style, type, from, to, duration, repeatCount){

        var p = document.createElementNS("http://www.w3.org/2000/svg","path");

        p.setAttribute("d",data);
        p.style=style;
        this.drawSpace.appendChild(p);

        this.addTransformAnimation(p, type, from, to, duration, repeatCount);

    };

    addTransformingPolygon(points, style, type, from, to, duration, repeatCount){

        var polygon = document.createElementNS("http://www.w3.org/2000/svg","polygon");

        polygon.setAttribute("points",points);
        polygon.style = style;

        this.drawSpace.appendChild(polygon);

        this.addTransformAnimation(polygon, type, from, to, duration, repeatCount);

    };

    addTransformingPolyline(points, style, type, from, to, duration, repeatCount){

        var polyline = document.createElementNS("http://www.w3.org/2000/svg","polyline");

        polyline.setAttribute("points",points);
        polyline.style = style;

        this.drawSpace.appendChild(polyline);

        this.addTransformAnimation(polyline, type, from, to, duration, repeatCount);

    };

    addTransformingText(text, x, y, style, type, from, to, duration, repeatCount){

        var t = document.createElementNS("http://www.w3.org/2000/svg","text");

        t.innerHTML = text;
        t.setAttribute("x",x);
        t.setAttribute("y",y);
        t.setAttribute("style",style);

        this.drawSpace.appendChild(t);

        this.addTransformAnimation(t, type, from, to, duration, repeatCount);

    };

    /**
     * 
     * @param {*} element       The SVG element to animate
     * @param {*} path          The path the element should follow in the animation            
     * @param {*} duration      The duration of the animation
     * @param {*} repeat        Determines how often the animation is repeated from 1 to indefinite
     */
    addMoveAnimation(element, path, duration, repeat){
        var animation = document.createElementNS("http://www.w3.org/2000/svg","animateMotion");
        animation.setAttribute("path", path);
        animation.setAttribute("dur", duration);
        animation.setAttribute("repeatCount",repeat); // 1 to indefinite

        element.appendChild(animation);
    }
    /**
     * 
     * @param {*} element       The SVG element to animate
     * @param {*} type          The type of transformation for example "rotate" or "scale"
     * @param {*} from          The start parameter of the animation
     * @param {*} to            The end parameter
     * @param {*} duration      The duration of the animation 
     * @param {*} repeatCount   Determines how often the animation is repeated from 1 to indefinite
     */
    addTransformAnimation(element, type, from, to , duration, repeatCount){
        var animation = document.createElementNS("http://www.w3.org/2000/svg","animateTransform");
        animation.setAttribute("attributeName","transform");
        animation.setAttribute("type",type);
        animation.setAttribute("from",from);
        animation.setAttribute("to",to);
        animation.setAttribute("dur",duration);
        animation.setAttribute("repeatCount",repeatCount);

        element.appendChild(animation);

    }

    getDrawSpaceWidth(){
        return this.drawSpace.getAttribute("width");
    };

    getDrawSpaceHeight(){
        return this.drawSpace.getAttribute("height");
    }

}


/**
 * The RandomGenerator allows the creation of randomised SVG Elements
 * currently supported elements:
 * circles with drawCircles(amount)
 */
class RandomGenerator{

    constructor(svgID){
	    this.svgID = svgID;
    };

    drawCircles(amount){

        var mySVGDrawer = new SVGDrawer(this.svgID);
        
        for(var i = 0; i < amount; i++){
            
            var cx = this.randomNumber(mySVGDrawer.getDrawSpaceWidth());
            var cy = this.randomNumber(mySVGDrawer.getDrawSpaceHeight());
            var temp = ( Math.floor(( parseInt( mySVGDrawer.getDrawSpaceWidth() ) + parseInt( mySVGDrawer.getDrawSpaceHeight() ) ) ) / 6 );
            var r  = this.randomNumber(temp);
            mySVGDrawer.addCircle(cx, cy, r,this.randomColor());

        }

    };

	drawRectangles(amount){

        var mySVGDrawer = new SVGDrawer(this.svgID);
        
        for(var i = 0; i < amount; i++){
            
            var x = this.randomNumber(mySVGDrawer.getDrawSpaceWidth() / 2);
            var y = this.randomNumber(mySVGDrawer.getDrawSpaceHeight() / 2);
            var width = this.randomNumber(mySVGDrawer.getDrawSpaceWidth());
            var height = this.randomNumber(mySVGDrawer.getDrawSpaceHeight());
            var rx = this.randomNumber(width);
            var ry = this.randomNumber(height);
            mySVGDrawer.addRectangle(x, y, rx, ry, width, height, this.randomColor());

        }
    }


    randomNumber(max){

        return Math.floor(Math.random() * max);

    };

    randomColor(){

        var r = this.randomNumber(255);
        var g = this.randomNumber(255);
        var b = this.randomNumber(255);

        return "fill:rgb(" + r + "," + g + "," + b + ");";


    };

}

class RandomAnimationGenerator{

    constructor(svgID){
	this.svgID = svgID;
    };

    drawCircles(amount,animation){

        var mySVGDrawer = new SVGAnimationBuilder(this.svgID);
        
        for(var i = 0; i < amount; i++){
            
            var cx = randomNumber(mySVGDrawer.getDrawSpaceWidth());
            var cy = randomNumber(mySVGDrawer.getDrawSpaceHeight());
            var temp = ( Math.floor(( parseInt( mySVGDrawer.getDrawSpaceWidth() ) + parseInt( mySVGDrawer.getDrawSpaceHeight() ) ) ) / 6 );
            var r  = randomNumber(temp);
            
            mySVGDrawer.addMovingCircle(cx, cy, r,this.randomColor(),animation.path, animation.duration, animation.repeatCount);

        }

    };

    drawCirclesRandomAnimation(amount){

        var mySVGDrawer = new SVGAnimationBuilder(this.svgID);
        
        for(var i = 0; i < amount; i++){
            
            var cx = randomNumber(mySVGDrawer.getDrawSpaceWidth());
            var cy = randomNumber(mySVGDrawer.getDrawSpaceHeight());
            var temp = ( Math.floor(( parseInt( mySVGDrawer.getDrawSpaceWidth() ) + parseInt( mySVGDrawer.getDrawSpaceHeight() ) ) ) / 6 );
            var r  = randomNumber(temp);
            var path = this.randomPath(cx,cy, mySVGDrawer.getDrawSpaceWidth(), mySVGDrawer.getDrawSpaceHeight(),5);
            var dur = randomNumber(50) + 1;
            var animation = new MoveAnimation(path,dur,"indefinite");
            
            mySVGDrawer.addMovingCircle(cx, cy, r,this.randomColor(), animation.path, animation.duration, animation.repeatCount);

        }

    };



    /**
     * Generates a random path starting at cx,cy with #length points
     * @param {*} cx 
     * @param {*} cy 
     * @param {*} max_X 
     * @param {*} max_Y 
     * @param {*} length 
     */
    randomPath(cx, cy, max_X, max_Y, length){

        var path = "M " + cx + " " + cy + " ";

        for(var i = 0; i < length; i++){

            var newX = randomNumberAdvanced(max_X);
            var newY = randomNumberAdvanced(max_Y);

            path += "L " + newX + " " + newY + " ";

        }

        path += " Z";

        return path;

    }

    randomColor(){

        var r = randomNumber(255);
        var g = randomNumber(255);
        var b = randomNumber(255);

        return "fill:rgb(" + r + "," + g + "," + b + ");";


    };

};

function randomNumber(max){

    return Math.floor(Math.random() * max);

};

function randomNumberAdvanced(max){

    var exit = false;
    var random = 0;

    while(exit == false){

        random = randomNumber(max) - randomNumber(max);

        var check = (-1) * random;

        if(random < 0){

            if(check < max){
                exit = true;
            }

        } else if(random < max){

            exit = true;

        }

    }

    return random;

}

function randomColor(){

    var r = randomNumber(255);
    var g = randomNumber(255);
    var b = randomNumber(255);

    return "rgb(" + r + "," + g + "," + b + ");";

};
