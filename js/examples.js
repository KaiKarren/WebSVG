/**
 * Examples of some of the features of the WebSVG API
 * created by Kai Karren
 * kaikarren.de
 */

function drawASimpleHouse(){
    var drawer = new SVGDrawer("mySvg");
    drawer.addRectangle(75,50,0,0,200,100,"fill:red");
    drawer.addPolygon("50,50 175,0 300,50","fill:red");
}

function animationExampleMovingCircle(){
    var myAnimationMaker = new SVGAnimationBuilder("mySvg");
    myAnimationMaker.addMovingCircle(50,50,60,"fill:#00FF00","M 0 0 L 100 100 Z","5s","indefinite");
}

function animationExampleMovingCircles(){
    var myRandom = new RandomAnimationGenerator("mySvg");
    myRandom.drawCircles(10, new MoveAnimation("M150 100 L 100 100 Z","5s","indefinite"));
}

function animationExampleDraw20randomCircles(){
    var myRandom = new RandomAnimationGenerator("mySvg");
    myRandom.drawCirclesRandomAnimation(20);
}

/*

More examples saved as comments


var test = new MoveAnimation("M150 100 L 100 100 Z","5s","10s");


var mySVGDrawer = new SVGDrawer("mySvg");
mySVGDrawer.addText("Kai Karren", 100, 100,"fill:#00FFFF");

mySVGDrawer.addCircle(50,50,60,"fill:red");
mySVGDrawer.addCircle(50,50,20,"fill:blue");
mySVGDrawer.addPath("M150 0 L75 200 L225 200 Z","fill:red");
mySVGDrawer.addEllipse(240,100,220,30,"fill:blue");
mySVGDrawer.addLine(0,0,200,200,"stroke:blue;stroke-width:3");
mySVGDrawer.addPolygon("200,10 250,190 160,210","fill:red");
mySVGDrawer.addPolyline("20,20 40,25 60,40 80,120 120,140 200,180","fill:none;stroke:black");


var myAnimationMaker = new SVGAnimationBuilder("mySvg");
myAnimationMaker.addMovingCircle(50,50,60,"fill:#00FF00","M 0 0 L 100 100 Z","5s","indefinite");
myAnimationMaker.addMovingCircle(50,50,60,"fill:#00FF00","M 50 50 L 250 450 L 1526 1052 Z","5s","indefinite");
myAnimationMaker.addMovingLine(50,50,100,100,"stroke:red;stroke-width:3","M 50 50 L 250 250 L 350 400 Z","10s","indefinite");
myAnimationMaker.addMovingEllipse(240,100,220,30,"fill:blue","M 240 100 L 500 500 L 0 0 Z", "10s","indefinite");
myAnimationMaker.addMovingPath("M150 0 L75 200 L225 200 Z","fill:red","M 150 0 L 200 400 L 800 800 Z", "10s", "indefinite");
myAnimationMaker.addMovingPolygon("200,10 250,190 160,210","fill:red","M200 10 L 1000 0 L 500 500 Z","5s","indefinite");
myAnimationMaker.addMovingPolyline("20,20 40,25 60,40 80,120 120,140 200,180","fill:none;stroke:black","M20 20 L 500 500 L 0 0 Z", "10s" ,"indefinite");
myAnimationMaker.addTransformingCircle(50,50,10,"fill:00FF00","scale","1","3","5s","indefinite");
myAnimationMaker.addTransformingEllipse(240, 100, 220, 30,"fill:blue","rotate","0","360","5s","indefinite");
myAnimationMaker.addTransformingPath("M150 0 L75 200 L225 200 Z","fill:red","rotate","0", "90", "5s", "indefinite");
myAnimationMaker.addTransformingPolygon("200,10 250,190 160,210","fill:lime","rotate","0", "360","5s","indefinite");
myAnimationMaker.addTransformingPolyline("20,20 40,25 60,40 80,120 120,140 200,180","fill:none;stroke:black","scale", "1", "2", "10s" ,"indefinite");
myAnimationMaker.addMovingText("Hallo",200,200,"fill:red","M 200 200 L 100 100 Z", "5s", "indefinite");
myAnimationMaker.addTransformingText("World",300,300,"fill:black","rotate","0","360","3s","indefinite");
*/